class Post < ActiveRecord::Base
  attr_accessible :conteudo, :nome, :titulo
  
  validates :nome,	:presence => true
  validates :titulo,	:presence => true,
			:length => { :minimum => 5}
  has_many  :comments,  :dependent => :destroy
end
